//
//  ViewController.swift
//  Dicy
//
//  Created by Stephen lee on 1/14/18.
//  Copyright © 2018 Stephen Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    
    let numOfDiceImages : UInt32 = 6
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDiceImages(numOfDiceImages: numOfDiceImages, diceImageView: diceImageView1)
        updateDiceImages(numOfDiceImages: numOfDiceImages, diceImageView: diceImageView2)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func rollButtonPressed(_ sender: UIButton) {
        updateDiceImages(numOfDiceImages: numOfDiceImages, diceImageView: diceImageView1)
        updateDiceImages(numOfDiceImages: numOfDiceImages, diceImageView: diceImageView2)
    }
    
    func updateDiceImages(numOfDiceImages: UInt32, diceImageView: UIImageView) {
        
        let randomDiceIndex = Int(arc4random_uniform(numOfDiceImages))
        
        diceImageView.image = UIImage(named: "dice\(randomDiceIndex + 1)")

    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        updateDiceImages(numOfDiceImages: numOfDiceImages, diceImageView: diceImageView1)
        updateDiceImages(numOfDiceImages: numOfDiceImages, diceImageView: diceImageView2)
    }
}






