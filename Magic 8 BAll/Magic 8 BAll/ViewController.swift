//
//  ViewController.swift
//  Magic 8 BAll
//
//  Created by Stephen lee on 1/14/18.
//  Copyright © 2018 Stephen Lee. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var askMeAnythingLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    let numOfBalls : UInt32 = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        randomBallNumber(numOfBalls: numOfBalls, ballImageView: imageView)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func askButtonPressed(_ sender: UIButton) {
        
        randomBallNumber(numOfBalls: numOfBalls, ballImageView: imageView)
    }
    
    func randomBallNumber(numOfBalls n: UInt32, ballImageView view: UIImageView) {
        let r =  arc4random_uniform(n)+1
        
        print(r)
        view.image = UIImage(named: "ball\(r)")
    }
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        randomBallNumber(numOfBalls: numOfBalls, ballImageView: imageView)
    }
}

