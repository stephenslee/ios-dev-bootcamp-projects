//
//  main.swift
//  Classes and Objects
//
//  Created by Stephen lee on 1/21/18.
//  Copyright © 2018 Stephen Lee. All rights reserved.
//

import Foundation

let myCar = SelfDrivingCar()
let sleeCar = Car(customerChosenColor: "Gold")
myCar.destination = "meh"
print(myCar.color)
print(myCar.numberOfSeats)
print(myCar.typeOfCar)
myCar.drive()
