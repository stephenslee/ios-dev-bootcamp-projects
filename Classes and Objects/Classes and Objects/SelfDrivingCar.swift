//
//  SelfDrivingCar.swift
//  Classes and Objects
//
//  Created by Stephen lee on 1/21/18.
//  Copyright © 2018 Stephen Lee. All rights reserved.
//

import Foundation

class SelfDrivingCar : Car {
    
    var destination : String?
    
    override func drive() {
        super.drive()
        
        // optional binding
        if let userSetDestination = destination {
            print("Driving towards " + userSetDestination)
        }
    }
}
