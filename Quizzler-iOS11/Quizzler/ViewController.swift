//
//  ViewController.swift
//  Quizzler
//
//  Created by Angela Yu on 25/08/2015.
//  Copyright (c) 2015 London App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Place your instance variables here
    
    let allQuestions = QuestionBank()
    var length : Int = 0
    var currentIndex : Int = 0
    var score : Int = 0
    var pickedAnswer : Bool = false
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var trueButton: UIButton!
    @IBOutlet weak var falseButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        length = allQuestions.list.count
        score = 0
        
        progressLabel.text = "\(currentIndex+1) / \(length)"
        scoreLabel.text = "Score: 0"
        questionLabel.text = allQuestions.list[currentIndex].questionText
        
    }


    @IBAction func answerPressed(_ sender: AnyObject) {
        if sender.tag == 1 {
            pickedAnswer = true
        } else if sender.tag == 2 {
            pickedAnswer = false
        }
        checkAnswer()
    }
    
    
    func updateUI() {
        progressLabel.text = "\(currentIndex+1) / 4"
        scoreLabel.text = "Score: \(score)"
        questionLabel.text = allQuestions.list[currentIndex].questionText
        progressBar.frame.size.width = (view.frame.size.width / CGFloat(length)) * CGFloat(currentIndex+1)
    }
    

    func nextQuestion() {
        currentIndex += 1
        if currentIndex == length {
            let alert = UIAlertController(title: "Awesome", message: "You've finished all the questions,do you want to start over?", preferredStyle: .alert)
            
            let restartAction = UIAlertAction(title: "Restart", style: .default, handler: {(UIAlertAction) in self.startOver()})
            
            alert.addAction(restartAction)
            present(alert, animated: true, completion: nil)
        } else {
            updateUI()
        }
    }
    
    
    func checkAnswer(){
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds

        if self.allQuestions.list[self.currentIndex].answer == self.pickedAnswer {
            self.questionLabel.text = "Correct"
            ProgressHUD.showSuccess("Correct")
            self.score += 1
        } else {
            questionLabel.text = "Incorrect"
            ProgressHUD.showError("Wrong")
        }
        
        self.trueButton.isEnabled = false
        self.falseButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: when) {
           
            self.trueButton.isEnabled = true
            self.falseButton.isEnabled = true
            self.nextQuestion()
        }
    }
    
    
    func startOver() {
        pickedAnswer = false
        currentIndex = 0
        updateUI()
    }
    

    
}
